library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity butterfly_testbench is
end butterfly_testbench;

architecture Behavioral of butterfly_testbench is

    -- Constants
    constant WIDTH : integer := 18;

    component butterfly is
        generic(
            WIDTH : natural := 18
        );
        port(
            clk         : in  std_logic;
            symbol_a_re : in  std_logic_vector(WIDTH - 1 downto 0);
            symbol_a_im : in  std_logic_vector(WIDTH - 1 downto 0);
            symbol_b_re : in  std_logic_vector(WIDTH - 1 downto 0);
            symbol_b_im : in  std_logic_vector(WIDTH - 1 downto 0);
            coeff_re    : in  std_logic_vector(WIDTH - 1 downto 0);
            coeff_im    : in  std_logic_vector(WIDTH - 1 downto 0);
            result_a_re : out std_logic_vector(WIDTH - 1 downto 0);
            result_a_im : out std_logic_vector(WIDTH - 1 downto 0);
            result_b_re : out std_logic_vector(WIDTH - 1 downto 0);
            result_b_im : out std_logic_vector(WIDTH - 1 downto 0)
        );
    end component;

    signal clk         : std_logic := '0';
    signal symbol_a_re : std_logic_vector(WIDTH - 1 downto 0);
    signal symbol_a_im : std_logic_vector(WIDTH - 1 downto 0);
    signal symbol_b_re : std_logic_vector(WIDTH - 1 downto 0);
    signal symbol_b_im : std_logic_vector(WIDTH - 1 downto 0);
    signal coeff_re    : std_logic_vector(WIDTH - 1 downto 0);
    signal coeff_im    : std_logic_vector(WIDTH - 1 downto 0);
    signal result_a_re : std_logic_vector(WIDTH - 1 downto 0);
    signal result_a_im : std_logic_vector(WIDTH - 1 downto 0);
    signal result_b_re : std_logic_vector(WIDTH - 1 downto 0);
    signal result_b_im : std_logic_vector(WIDTH - 1 downto 0);

begin

UUT: butterfly
    port map(
        clk         => clk,
        symbol_a_re => symbol_a_re,
        symbol_a_im => symbol_a_im,
        symbol_b_re => symbol_b_re,
        symbol_b_im => symbol_b_im,
        coeff_re    => coeff_re,
        coeff_im    => coeff_im,
        result_a_re => result_a_re,
        result_a_im => result_a_im,
        result_b_re => result_b_re,
        result_b_im => result_b_im
    );

-- Clock generation
clock: process
begin
    clk <= not clk;
    wait for 5 ns;
end process clock;

-- Testbench stimuli
tb_stim: process
begin

    symbol_a_re <= "010000000000000000";
    symbol_a_im <= "000000000000000000";
    symbol_b_re <= "010000000000000000";
    symbol_b_im <= "000000000000000000";
    coeff_re    <= "010000000000000000";
    coeff_im    <= "000000000000000000";
    
    wait for 600 ns;

    symbol_a_re <= "001000000000000000";
    symbol_a_im <= "001000000000000000";
    symbol_b_re <= "001000000000000000";
    symbol_b_im <= "001000000000000000";
    coeff_re    <= "010000000000000000";
    coeff_im    <= "000000000000000000";
    
    wait for 600 ns;

    symbol_a_re <= "000000000000000000";
    symbol_a_im <= "010000000000000000";
    symbol_b_re <= "010000000000000000";
    symbol_b_im <= "001000000000000000";
    coeff_re    <= "010000000000000000";
    coeff_im    <= "000000000000000000";

    wait;
    
end process tb_stim;


end Behavioral;
