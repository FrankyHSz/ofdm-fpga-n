library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity complex_mult_testbench is
end complex_mult_testbench;


architecture Behavioral of complex_mult_testbench is

--    -- Unit under test
--    component complex_mult is
--        port (
--             --rst: in  std_logic;
--             clk: in  std_logic;
--            a_re: in  std_logic_vector(17 downto 0);  -- Complex amplitude's real part
--            a_im: in  std_logic_vector(17 downto 0);  -- Complex amplitude's imag. part
--            b_re: in  std_logic_vector(17 downto 0);  -- IFFT coefficient's real part
--            b_im: in  std_logic_vector(17 downto 0);  -- IFFt coefficient's imag. part
--            m_re: out std_logic_vector(17 downto 0);  -- Output's real part
--            m_im: out std_logic_vector(17 downto 0)   -- Output's imag. part
--              );
--    end component;

    -- Unit signals
    --signal rst_sig: std_logic;
    signal clk_sig  : std_logic := '0';
    signal a_re     : std_logic_vector(17 downto 0);  -- Complex amplitude's real part
    signal a_im     : std_logic_vector(17 downto 0);  -- Complex amplitude's imag. part
    signal b_re     : std_logic_vector(17 downto 0);  -- IFFT coefficient's real part
    signal b_im     : std_logic_vector(17 downto 0);  -- IFFt coefficient's imag. part
    signal m_re_ext : std_logic_vector(36 downto 0);  -- Output's real part
    signal m_im_ext : std_logic_vector(36 downto 0);  -- Output's imag. part
    signal m_re     : std_logic_vector(17 downto 0);  -- Output's real part
    signal m_im     : std_logic_vector(17 downto 0);  -- Output's imag. part

    component complex_mult is
    generic(
        AWIDTH : natural := 18;
        BWIDTH : natural := 18 );
    port(
        clk    : in  std_logic;
        ar, ai : in  std_logic_vector(AWIDTH - 1      downto 0);
        br, bi : in  std_logic_vector(BWIDTH - 1      downto 0);
        pr, pi : out std_logic_vector(AWIDTH + BWIDTH downto 0));
    end component;

begin

--UUT: complex_mult
--    port map(
--         --rst => rst_sig,
--         clk => clk_sig,
--        a_re => a_re,
--        a_im => a_im,
--        b_re => b_re,
--        b_im => b_im,
--        m_re => m_re,
--        m_im => m_im
--    );

UUT: complex_mult
    port map(
        clk => clk_sig,
        ar  => a_re, 
        ai  => a_im,
        br  => b_re, 
        bi  => b_im,
        pr  => m_re_ext, 
        pi  => m_im_ext
    );

-- Clock generation
clock: process
begin
    clk_sig <= not clk_sig;
    wait for 5 ns;
end process clock;

process(clk_sig)
begin
    if rising_edge(clk_sig) then
        m_re <= m_re_ext(33 downto 16);
        m_im <= m_im_ext(33 downto 16);
    end if;
end process;

-- Testbench stimuli
tb_stim: process
begin

--    rst_sig <= '1';
--    wait for 100 ns;
--    rst_sig <= '0';

    -- A = 0.5 + 0j
    a_re <= "001000000000000000";  -- 08000h
    a_im <= "000000000000000000";  -- 00000h
    
    -- B = 0.5 + 0j
    b_re <= "001000000000000000";  -- 08000h
    b_im <= "000000000000000000";  -- 00000h
    
    wait for 100 ns;  -- Expect 0.25 + 0j, 04000h + j*00000h

    -- A = 0 + 0.5j
    a_re <= "000000000000000000";  -- 00000h
    a_im <= "001000000000000000";  -- 08000h
    
    -- B = 0 + 0.5j
    b_re <= "000000000000000000";  -- 00000h
    b_im <= "001000000000000000";  -- 08000h
    
    wait for 100 ns;  -- Expect -0.25 + 0j, 3c000h + j*04000h

    -- A = 0.5 + 0.5j
    a_re <= "001000000000000000";  -- 08000h
    a_im <= "001000000000000000";  -- 08000h
    
    -- B = 0.5 + 0.5j
    b_re <= "001000000000000000";  -- 08000h
    b_im <= "001000000000000000";  -- 08000h
    
    wait;  -- Expect 0 + 0.5j, 00000h + j*08000h
    
end process tb_stim;

end Behavioral;
