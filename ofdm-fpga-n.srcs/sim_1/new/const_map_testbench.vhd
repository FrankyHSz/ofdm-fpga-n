library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity const_map_testbench is
end const_map_testbench;

architecture Behavioral of const_map_testbench is

    -- Unit under test
    component constellation_map is
        port(
            clk        : in  std_logic;
            enable     : in  std_logic;
            bit_string : in  std_logic_vector(5 downto 0);
            symbol_re  : out std_logic_vector(17 downto 0);
            symbol_im  : out std_logic_vector(17 downto 0)
            );
    end component;

    -- Unit signals
    signal clk        : std_logic := '0';
    signal enable     : std_logic;
    signal bit_string : std_logic_vector(5 downto 0);
    signal symbol_re  : std_logic_vector(17 downto 0);
    signal symbol_im  : std_logic_vector(17 downto 0);

begin

-- Unit under test
UUT: constellation_map
    port map(
        clk => clk,
        enable => enable,
        bit_string => bit_string,
        symbol_re => symbol_re,
        symbol_im => symbol_im
    );

-- Clock generation
clock: process
begin
    clk <= not clk;
    wait for 5 ns;
end process clock;
    
-- Testbench stimuli
tb_stim: process
begin

    enable <= '1';
    bit_string <= "000000";  -- 00h
    
    wait for 100 ns;  -- Expect 1/sqrt(2) + j*1/sqrt(2), 0B504h + j*0B504h

    bit_string <= "111111";  -- 3Fh
    
    wait for 100 ns;  -- Expect -1/sqrt(2) - j*1/sqrt(2), 11_0100_1010_1111_1100 34AFCh + j*34AFCh

    bit_string <= "011000";  -- 18h

    wait;  -- Expect 3E624h + j*04D94h

end process tb_stim;

end Behavioral;
