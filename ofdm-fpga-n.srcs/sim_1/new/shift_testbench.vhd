library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift_testbench is
end shift_testbench;

architecture Behavioral of shift_testbench is

    -- Constants
    constant width  : integer := 18;
    constant length : integer := 6;

    -- Unit under test
    component shift is
        generic(
            DATA_WIDTH : natural := width;
            SHR_LENGTH : natural := length
        );
        port(
            clk      : in  std_logic;
            data_in  : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
            data_out : out std_logic_vector(DATA_WIDTH - 1 downto 0)
        );
    end component;
    
    -- Unit signals
    signal clk_sig  : std_logic := '0';
    signal input_vector  : std_logic_vector(width - 1 downto 0) := (others => '0');
    signal output_vector : std_logic_vector(width - 1 downto 0) := (others => '0');
    
begin

-- Unit under test
UUT: shift
    port map(
        clk      => clk_sig,
        data_in  => input_vector,
        data_out => output_vector
    );
    
-- Clock generation
clock: process
begin
    clk_sig <= not clk_sig;
    wait for 5 ns;
end process clock;

-- Testbench stimuli
tb_stim: process
begin

    wait for 5 ns;

    input_vector <= (others => '1');
    
    wait for 10 ns;
    
    input_vector <= (others => '0');
    
    wait for 10 ns;
    
    input_vector <= (others => '0');
    
    wait for 10 ns;

    wait;

end process tb_stim;

end Behavioral;
