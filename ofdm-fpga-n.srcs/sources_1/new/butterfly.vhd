library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity butterfly is
    generic(
        WIDTH : natural := 18
    );
    port(
        clk         : in  std_logic;
        symbol_a_re : in  std_logic_vector(WIDTH - 1 downto 0);
        symbol_a_im : in  std_logic_vector(WIDTH - 1 downto 0);
        symbol_b_re : in  std_logic_vector(WIDTH - 1 downto 0);
        symbol_b_im : in  std_logic_vector(WIDTH - 1 downto 0);
        coeff_re    : in  std_logic_vector(WIDTH - 1 downto 0);
        coeff_im    : in  std_logic_vector(WIDTH - 1 downto 0);
        result_a_re : out std_logic_vector(WIDTH - 1 downto 0);
        result_a_im : out std_logic_vector(WIDTH - 1 downto 0);
        result_b_re : out std_logic_vector(WIDTH - 1 downto 0);
        result_b_im : out std_logic_vector(WIDTH - 1 downto 0)
    );
end butterfly;

architecture Behavioral of butterfly is

    -- Constants
    constant LENGTH : integer := 6;

    -- Used component #1: Complex multiplier
    component complex_mult is
        generic(
            AWIDTH : natural := WIDTH;
            BWIDTH : natural := WIDTH
        );
        port(
            clk    : in  std_logic;
            ar, ai : in  std_logic_vector(AWIDTH - 1      downto 0);
            br, bi : in  std_logic_vector(BWIDTH - 1      downto 0);
            pr, pi : out std_logic_vector(AWIDTH + BWIDTH downto 0)
        );
    end component;
    
    -- Used component #2: Shift register
    component shift is
        generic(
            DATA_WIDTH : natural := WIDTH;
            SHR_LENGTH : natural := LENGTH
        );
        port(
            clk      : in  std_logic;
            data_in  : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
            data_out : out std_logic_vector(DATA_WIDTH - 1 downto 0)
        );
    end component;
    
    -- Unit signals
    signal symbol_a_re_d   : std_logic_vector(WIDTH - 1 downto 0);
    signal symbol_a_im_d   : std_logic_vector(WIDTH - 1 downto 0);
    signal symbol_a_re_ext : std_logic_vector(WIDTH     downto 0);
    signal symbol_a_im_ext : std_logic_vector(WIDTH     downto 0);
    
    signal pr_out  : std_logic_vector(2 * WIDTH downto 0);
    signal pi_out  : std_logic_vector(2 * WIDTH downto 0);
    
    signal prod_re     : signed(WIDTH - 1 downto 0);
    signal prod_im     : signed(WIDTH - 1 downto 0);
    signal prod_re_ext : signed(WIDTH     downto 0);  -- Sign extended versions
    signal prod_im_ext : signed(WIDTH     downto 0);  -- Sign extended versions
    
    signal sum1_re : signed(WIDTH downto 0);
    signal sum1_im : signed(WIDTH downto 0);
    signal sum2_re : signed(WIDTH downto 0);
    signal sum2_im : signed(WIDTH downto 0);
    
    signal sum2_re_temp : signed(WIDTH - 1 downto 0);
    signal sum2_im_temp : signed(WIDTH - 1 downto 0);

begin

-- Instantiating a complex multiplier to multiply symbol "B" with the W_N Fourier coefficient
mult: complex_mult
    port map(
        clk => clk,
        ar  => symbol_b_re,
        ai  => symbol_b_im,
        br  => coeff_re,
        bi  => coeff_im,
        pr  => pr_out,
        pi  => pi_out
    );

-- Instantiating shift registers to compensate the 6 clock cycle delay of the complex multiplier
delayline_re: shift
    port map(
        clk      => clk,
        data_in  => symbol_a_re,
        data_out => symbol_a_re_d
    );

delayline_im: shift
    port map(
        clk      => clk,
        data_in  => symbol_a_im,
        data_out => symbol_a_im_d
    );
    
-- Assigning the most significant bits of the multiplier output to the "product" signal
process(clk)
begin

    if rising_edge(clk) then
        prod_re     <= signed(pr_out(2 * WIDTH - 3 downto WIDTH - 2));
        prod_re_ext <= (prod_re(WIDTH-1) & prod_re);                -- Sign extension for later addition (sum1)
        prod_im     <= signed(pi_out(2 * WIDTH - 3 downto WIDTH - 2));
        prod_im_ext <= (prod_im(WIDTH-1) & prod_im);                -- Sign extension for later addition (sum1)
    end if;

end process;

-- Adding and subtracting the two symbols (one multiplied with a Fourier coefficent)
process(clk)
begin

    if rising_edge(clk) then
        symbol_a_re_ext <= (symbol_a_re_d(WIDTH-1) & symbol_a_re_d);    -- Sign extension for sum1
        symbol_a_im_ext <= (symbol_a_im_d(WIDTH-1) & symbol_a_im_d);    -- Sign extension for sum1
        
        sum1_re <= signed(symbol_a_re_ext) + prod_re_ext;
        sum1_im <= signed(symbol_a_im_ext) + prod_im_ext;
        
        sum2_re_temp <= signed(symbol_a_re_d) - prod_re;
        sum2_im_temp <= signed(symbol_a_im_d) - prod_im;
        
        sum2_re <= (sum2_re_temp(WIDTH-1) & sum2_re_temp);
        sum2_im <= (sum2_im_temp(WIDTH-1) & sum2_im_temp);
    end if;

end process;

-- Assigning the output ports and also scaling them down by 2
process(clk)
begin

    if rising_edge(clk) then
        result_a_re <= std_logic_vector(sum1_re(WIDTH downto 1));
        result_a_im <= std_logic_vector(sum1_im(WIDTH downto 1));
        
        result_b_re <= std_logic_vector(sum2_re(WIDTH downto 1));
        result_b_im <= std_logic_vector(sum2_im(WIDTH downto 1));
    end if;

end process;

end Behavioral;
