--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;

---- Uncomment the following library declaration if using
---- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx leaf cells in this code.
---- library UNISIM;
---- use UNISIM.VComponents.all;

--entity complex_mult is
--    port (
--        clk  : in  std_logic;
--        a_re : in  std_logic_vector(17 downto 0);  -- Complex amplitude's real part
--        a_im : in  std_logic_vector(17 downto 0);  -- Complex amplitude's imag. part
--        b_re : in  std_logic_vector(17 downto 0);  -- IFFT coefficient's real part
--        b_im : in  std_logic_vector(17 downto 0);  -- IFFt coefficient's imag. part
--        m_re : out std_logic_vector(17 downto 0);  -- Output's real part
--        m_im : out std_logic_vector(17 downto 0)   -- Output's imag. part
--          );
--end complex_mult;

--architecture Behavioral of complex_mult is
--    signal in_reg_a_re : signed(17 downto 0);
--    signal in_reg_a_im : signed(17 downto 0);
--    signal in_reg_b_re : signed(17 downto 0);
--    signal in_reg_b_im : signed(17 downto 0);
    
--    signal preadd_1    : signed(17 downto 0);
--    signal partial_res : signed(35 downto 0);
--    signal result_re   : signed(35 downto 0);
--    signal result_im   : signed(35 downto 0);
    
--    -- To see how DSP slices are used, read 
--    -- the description below
----    attribute USE_DSP: string;
----    attribute USE_DSP of partial_res : signal is "YES";
----    attribute USE_DSP of result_re   : signal is "YES";
----    attribute USE_DSP of result_im   : signal is "YES";
    
--    attribute USE_DPORT: boolean;
--    attribute USE_DPORT of in_reg_a_im: signal is TRUE;
--begin

---- Input sampling
--process(clk)
--begin
--    if(rising_edge(clk)) then
--        in_reg_a_re <= signed(a_re);
--        in_reg_a_im <= signed(a_im);
--        in_reg_b_re <= signed(b_re);
--        in_reg_b_im <= signed(b_im);
--    end if;
--end process;

---- First product: "(a-b)*d"
--process(clk)
--begin
--    if(rising_edge(clk)) then
--        preadd_1 <= (in_reg_a_re - in_reg_a_im);
--        partial_res <= preadd_1 * in_reg_b_im;
--    end if;
--end process;

---- Output assignment
--process(clk)
--begin
--    if(rising_edge(clk)) then
--        m_re <= std_logic_vector(partial_res(33 downto 16));
--        --m_re <= std_logic_vector(result_re(33 downto 16));
--    end if;
--end process;

--end Behavioral;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity complex_mult is
    generic(
        AWIDTH : natural := 18;
        BWIDTH : natural := 18
    );
    port(
        clk    : in  std_logic;
        ar, ai : in  std_logic_vector(AWIDTH - 1      downto 0);
        br, bi : in  std_logic_vector(BWIDTH - 1      downto 0);
        pr, pi : out std_logic_vector(AWIDTH + BWIDTH downto 0)
    );
end complex_mult;

architecture rtl of complex_mult is
    signal ai_d, ai_dd, ai_ddd, ai_dddd : signed(AWIDTH - 1      downto 0);
    signal ar_d, ar_dd, ar_ddd, ar_dddd : signed(AWIDTH - 1      downto 0);
    signal bi_d, bi_dd, bi_ddd          : signed(BWIDTH - 1      downto 0);
    signal br_d, br_dd, br_ddd          : signed(BWIDTH - 1      downto 0);
    signal add_common                   : signed(AWIDTH          downto 0);
    signal addr, addi                   : signed(AWIDTH          downto 0);
    signal mult0, multr, multi          : signed(AWIDTH + BWIDTH downto 0);
    signal pr_int, pi_int               : signed(AWIDTH + BWIDTH downto 0);
    signal common, commonr1, commonr2   : signed(AWIDTH + BWIDTH downto 0);
begin

    process(clk)
    begin
        if rising_edge(clk) then
            ar_d   <= signed(ar);
            ar_dd  <= signed(ar_d);
            ai_d   <= signed(ai);
            ai_dd  <= signed(ai_d);
            br_d   <= signed(br);
            br_dd  <= signed(br_d);
            br_ddd <= signed(br_dd);
            bi_d   <= signed(bi);
            bi_dd  <= signed(bi_d);
            bi_ddd <= signed(bi_dd);
        end if;
    end process;
    
    -- Common factor (ar -ai) x bi, shared for the calculations
    -- of the real and imaginary final products.
    --
    process(clk)
    begin
        if rising_edge(clk) then
            add_common <= resize(ar_d, AWIDTH + 1) - resize(ai_d, AWIDTH + 1);
            mult0      <= add_common * bi_dd;
            common     <= mult0;
        end if;
    end process;
    
    -- Real product
    --
    process(clk)
    begin
        if rising_edge(clk) then
            ar_ddd   <= ar_dd;
            ar_dddd  <= ar_ddd;
            addr     <= resize(br_ddd, BWIDTH + 1) - resize(bi_ddd, BWIDTH + 1);
            multr    <= addr * ar_dddd;
            commonr1 <= common;
            pr_int   <= multr + commonr1;
        end if;
    end process;
    
    -- Imaginary product
    --
    process(clk)
    begin
        if rising_edge(clk) then
            ai_ddd   <= ai_dd;
            ai_dddd  <= ai_ddd;
            addi     <= resize(br_ddd, BWIDTH + 1) + resize(bi_ddd, BWIDTH + 1);
            multi    <= addi * ai_dddd;
            commonr2 <= common;
            pi_int   <= multi + commonr2;
        end if;
    end process;
    
    --
    -- VHDL type conversion for output
    --
    pr <= std_logic_vector(pr_int);
    pi <= std_logic_vector(pi_int);
    
end rtl;