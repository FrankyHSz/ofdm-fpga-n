library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- To read initialization data from text file
use STD.textio.all;

entity constellation_map is
   port(
       clk        : in  std_logic;
       enable     : in  std_logic;
       bit_string : in  std_logic_vector(5 downto 0);
       symbol_re  : out std_logic_vector(17 downto 0);
       symbol_im  : out std_logic_vector(17 downto 0)
   );
end constellation_map;

architecture Behavioral of constellation_map is
    type QAM_64_const_map is array (63 downto 0) 
        of std_logic_vector(17 downto 0);
        
    impure function initBRAM(file_name : string) return QAM_64_const_map is
        -- Variables needed to read initialization file
        file     constellation_init : text;
        variable            in_line : line;
        variable            in_char : character;
        variable              value : std_logic_vector(17 downto 0);
        variable        output_BRAM : QAM_64_const_map;
    begin
    
        file_open(constellation_init, file_name, read_mode);

        for idx in 0 to 63 loop
        
            -- Reading one line of data from each init. file
            if (not endfile(constellation_init)) then
                readline(constellation_init, in_line);
            end if;
        
            for bit_pos in 17 downto 0 loop
                
                -- Reading one character from actual line
                read(in_line, in_char);
                
                -- Setting actual bit position based on actual character
                if (in_char = '0') then
                    value(bit_pos) := '0';
                elsif (in_char = '1') then
                    value(bit_pos) := '1';
                else
                    report "Invalid character!" severity ERROR;
                end if;
            
            end loop;
            
            output_BRAM(idx) := value;
            
        end loop;
    
        file_close(constellation_init);
        
        return output_BRAM;
    
    end function;


    shared variable real_part : QAM_64_const_map := initBRAM("/home/frankyhsz/MSc/1.felev/Onlab1/ofdm-fpga-n/onlab1/constellation_init_real.txt");
    shared variable imag_part : QAM_64_const_map := initBRAM("/home/frankyhsz/MSc/1.felev/Onlab1/ofdm-fpga-n/onlab1/constellation_init_imag.txt");
    
    signal symbol_re_reg: std_logic_vector(17 downto 0);
    signal symbol_im_reg: std_logic_vector(17 downto 0);
begin

-- Mapping bit strings into complex symbols by
-- "indexing" constellation map with input bit strings
process(clk)
    variable idx : integer range 0 to 63;
begin
    if(rising_edge(clk)) then
        if(enable = '1') then
            idx := to_integer(unsigned(bit_string));
            symbol_re_reg <= real_part(idx);
            symbol_im_reg <= imag_part(idx);
        end if;
    end if;
end process;

-- Simulating BRAM output delay of one clock cycle
-- Only for simulation?
process(clk)
begin
    symbol_re <= symbol_re_reg;
    symbol_im <= symbol_im_reg;
end process;

end Behavioral;
