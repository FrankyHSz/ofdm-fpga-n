library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package my_pack is
    type complex_num_18bit is array(0 to 1)
        of std_logic_vector(17 downto 0);
    type complex_num_vector_18bit is array(natural range<>)
        of complex_num_18bit;
    type multistage_cmplx_n_18bit is array(natural range<>, natural range<>)
        of complex_num_18bit;    
    
end package;
