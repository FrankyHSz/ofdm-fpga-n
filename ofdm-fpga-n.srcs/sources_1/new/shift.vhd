library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift is
    generic(
        DATA_WIDTH : natural := 18;
        SHR_LENGTH : natural := 6
    );
    port(
        clk      : in  std_logic;
        data_in  : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
        data_out : out std_logic_vector(DATA_WIDTH - 1 downto 0)
    );
end shift;

architecture Behavioral of shift is

    -- Delay line type
    type delay_line_t is array(0 to SHR_LENGTH - 1) of std_logic_vector(DATA_WIDTH - 1 downto 0);
    
    -- Delay line instance
    signal delay_line : delay_line_t;

begin

process(clk)
begin
    if rising_edge(clk) then
        delay_line(0) <= data_in;
        for idx in 1 to (SHR_LENGTH - 1) loop
            delay_line(idx) <= delay_line(idx-1);
        end loop;
        data_out <= delay_line(SHR_LENGTH - 1);
    end if;
end process;

end Behavioral;
