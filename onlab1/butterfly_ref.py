################################################################################

# --- Script information --- #

# Title :
#   Butterfly reference
#
# Description:
#   Computes a butterfly operation for reference

################################################################################


# --- Imports --- #

from FixedPoint import FXfamily, FXnum      # Fixed point arithmetic package
import scipy                                # Scientific Python
from bitstring import BitArray              # Binary string to int conversion
import random                               # For random input vector gen.


# --- Number representation --- #

fractional_bits = 16
sign_plus_integer_bits = 2                  # One sign bit, one integer bit
fam_s_1_16 = FXfamily(fractional_bits, sign_plus_integer_bits)


# --- Generating the input numbers --- #

num_of_input_vectors = 10
input_vectors_a_re = list(range(num_of_input_vectors))
input_vectors_a_im = list(range(num_of_input_vectors))
input_vectors_b_re = list(range(num_of_input_vectors))
input_vectors_b_im = list(range(num_of_input_vectors))
input_vectors_w_re = list(range(num_of_input_vectors))
input_vectors_w_im = list(range(num_of_input_vectors))

for idx in range(num_of_input_vectors):
    input_vectors_a_re[idx] = random.uniform(-1, 1)
    input_vectors_a_im[idx] = random.uniform(-1, 1)
    input_vectors_b_re[idx] = random.uniform(-1, 1)
    input_vectors_b_im[idx] = random.uniform(-1, 1)
    coeff = random.uniform(-2*scipy.pi, 2*scipy.pi)
    input_vectors_w_re[idx] = scipy.cos(coeff)
    input_vectors_w_im[idx] = scipy.sin(coeff)

# Output file
f_input = open("input_numbers_bf.txt", "w")

f_input.write(str(num_of_input_vectors) + '\n')

for idx in range(num_of_input_vectors):

    # Saving the results into the output file
    sym_a_fp_re = FXnum(input_vectors_a_re[idx], fam_s_1_16)
    sym_a_fp_im = FXnum(input_vectors_a_im[idx], fam_s_1_16)
    sym_b_fp_re = FXnum(input_vectors_b_re[idx], fam_s_1_16)
    sym_b_fp_im = FXnum(input_vectors_b_im[idx], fam_s_1_16)
    coe_w_fp_re = FXnum(input_vectors_w_re[idx], fam_s_1_16)
    coe_w_fp_im = FXnum(input_vectors_w_im[idx], fam_s_1_16)
    sym_a_fp_re = sym_a_fp_re.toBinaryString().replace('.', '')
    sym_a_fp_im = sym_a_fp_im.toBinaryString().replace('.', '')
    sym_b_fp_re = sym_b_fp_re.toBinaryString().replace('.', '')
    sym_b_fp_im = sym_b_fp_im.toBinaryString().replace('.', '')
    coe_w_fp_re = coe_w_fp_re.toBinaryString().replace('.', '')
    coe_w_fp_im = coe_w_fp_im.toBinaryString().replace('.', '')
    f_input.write(sym_a_fp_re + '\n')
    f_input.write(sym_a_fp_im + '\n')
    f_input.write(sym_b_fp_re + '\n')
    f_input.write(sym_b_fp_im + '\n')
    f_input.write(coe_w_fp_re + '\n')
    f_input.write(coe_w_fp_im + '\n')

f_input.close()


# --- Reading back the input numbers --- #

f_input = open("input_numbers_bf.txt", "r")
f_output = open("output_numbers_bf.txt", "w")

# In the first line there is an integer number that tells us how many number
# combinations are in the file
first_row = f_input.readline()
N = int(first_row)

for idx in range(N):

    # Symbol "A"
    line_re = f_input.readline()
    line_im = f_input.readline()

    line_re = BitArray(bin=line_re)
    line_im = BitArray(bin=line_im)

    a_val = float(line_re.int) / 2**16
    a_val += 1j * (float(line_im.int) / 2 ** 16)

    # Symbol "B"
    line_re = f_input.readline()
    line_im = f_input.readline()

    line_re = BitArray(bin=line_re)
    line_im = BitArray(bin=line_im)

    b_val = float(line_re.int) / 2 ** 16
    b_val += 1j * (float(line_im.int) / 2 ** 16)

    # Symbol "W": Fourier coefficient
    line_re = f_input.readline()
    line_im = f_input.readline()

    line_re = BitArray(bin=line_re)
    line_im = BitArray(bin=line_im)

    w_val = float(line_re.int) / 2 ** 16
    w_val += 1j * (float(line_im.int) / 2 ** 16)

    # Butterfly operation
    res_a = a_val + b_val * w_val
    res_a /= 2
    res_b = a_val - b_val * w_val
    res_b /= 2

    # Saving the results into the output file
    res_a_fp_re = FXnum(scipy.real(res_a), fam_s_1_16).toBinaryString(logBase=4)
    res_a_fp_im = FXnum(scipy.imag(res_a), fam_s_1_16).toBinaryString(logBase=4)
    res_b_fp_re = FXnum(scipy.real(res_b), fam_s_1_16).toBinaryString(logBase=4)
    res_b_fp_im = FXnum(scipy.imag(res_b), fam_s_1_16).toBinaryString(logBase=4)
    res_a_fp_re = res_a_fp_re.replace('.', '')
    res_a_fp_im = res_a_fp_im.replace('.', '')
    res_b_fp_re = res_b_fp_re.replace('.', '')
    res_b_fp_im = res_b_fp_im.replace('.', '')
    f_output.write("Result A real part = " + res_a_fp_re + '\n')
    f_output.write("Result A imag part = " + res_a_fp_im + '\n')
    f_output.write("Result B real part = " + res_b_fp_re + '\n')
    f_output.write("Result B imag part = " + res_b_fp_im + '\n')
    f_output.write('\n')


# --- Clean up --- #

f_input.close()
f_output.close()


# --- End of script --- #
