################################################################################

# --- Script information --- #

# Title :
#   FFT/IFFT coefficient generator
#
# Description:
#   Generates coefficients of N-point FFT/IFFT.

################################################################################


# --- Imports --- #

from FixedPoint import FXfamily, FXnum      # Fixed point arithmetic package
import math                                 # Mathematical functions (log2)
import pygame                               # Drawing the diagram


# --- Number representation --- #

fractional_bits = 16
sign_plus_integer_bits = 2                  # One sign bit, one integer bit
fam_s_1_16 = FXfamily(fractional_bits, sign_plus_integer_bits)

# --- Coefficient generation, saving and plotting --- #

# Initialization for later plotting
pygame.init()
screen = pygame.display.set_mode([600, 600])
screen.fill((255, 255, 255))
pygame.draw.line(screen, (0, 0, 0), [0, 300], [600, 300])
pygame.draw.line(screen, (0, 0, 0), [300, 0], [300, 600])

# Number of FFT/IFFT points
N = 64

# Output files
f_real = open("ifft_coeffs_" + str(N) + "_real.txt", "w")
f_imag = open("ifft_coeffs_" + str(N) + "_imag.txt", "w")

for idx in range(N):

    # Real and imaginary part generation
    coeff_re = math.cos(2*math.pi * idx / N)
    coeff_im = math.sin(2*math.pi * idx / N)

    # Quantization
    coeff_re_fp = FXnum(coeff_re, fam_s_1_16)
    coeff_im_fp = FXnum(coeff_im, fam_s_1_16)

    # Plotting
    xPos = float(coeff_re_fp.toDecimalString()) * 200 + 300
    yPos = float(coeff_im_fp.toDecimalString()) * 200 + 300
    pygame.draw.circle(screen, (255, 0, 0), [int(xPos), int(yPos)], 5)

    # Conversion into binary string
    coeff_re_fp = coeff_re_fp.toBinaryString().replace('.', '')
    coeff_im_fp = coeff_im_fp.toBinaryString().replace('.', '')

    # Saving to file
    f_real.write(coeff_re_fp + '\n')
    f_imag.write(coeff_im_fp + '\n')

pygame.draw.circle(screen,
                   (128, 128, 128),
                   [300, 300],
                   200,
                   1)

done = False
while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    pygame.display.flip()

pygame.quit()


# --- End of script --- #
