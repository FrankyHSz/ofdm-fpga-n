################################################################################

# --- Script information --- #

# Title :
#   Constellation diagram point generator script.
#
# Description:
#   Generates the real and imaginary parts of constellation diagram points and
#   saves them into a file readable for VHDL constructs.
#
################################################################################

# --- Imports --- #
from FixedPoint import FXfamily, FXnum          # Fixed point package
import numpy                                    # Numerical computation package
import pygame                                   # Drawing the const. diagram
from bitstring import BitArray                  # Bit string to integer conv.


# --- Constants, flags --- #
# Verification / Composite-diagram flag
# If True: script displays reference constellation diagram with black dots and
# fixed point approximation with red dots.
# If False: script displays "composite" constellation diagram with black dots
# used only in 64-QAM, blue dots used both in 16- and 64-QAM and red dots used
# only in 4-QAM.
verification_flag = True

# QAM flags
# If the verification flag is False (composite diagram selected) then it sets
# which point should be drawn.
# - QAM_4: enables only 4-QAM points to be seen.
# - QAM_16: enables 16-QAM points (includes 4-QAM points) to be seen.
# - QAM_64: enables all points.
# Because of flags of higher point numbers enable lower point numbers' points,
# all possible combinations are (X means "don't care"):
#
# QAM_4 | QAM_16 | QAM_64
# True  | False  | False
#   X   | True   | False
#   X   |   X    | True
#
QAM_4 = True
QAM_16 = False
QAM_64 = False


# --- Helper functions --- #
def to_binary_string(integer_value):
    fam_1_7bit_int = FXfamily(1, 7)
    almost_done = FXnum(integer_value, fam_1_7bit_int).toBinaryString()
    return almost_done[1:7]


# --- Fixed point representation --- #

fractional_bits = 16
sign_plus_integer_bits = 2                      # One sign bit, two integer bits

fam_s_1_16 = FXfamily(fractional_bits, sign_plus_integer_bits)
fam_s_18 = FXfamily(1, 18)                      # Helper family


# --- Constellation diagram --- #

pygame.init()
screen = pygame.display.set_mode([600, 600])
screen.fill((255, 255, 255))
pygame.draw.line(screen, (0, 0, 0), [0, 300], [600, 300])
pygame.draw.line(screen, (0, 0, 0), [300, 0], [300, 600])
pygame.font.init()

f_real = open("constellation_init_real.txt", "w")
f_imag = open("constellation_init_imag.txt", "w")

xCoord, yCoord = 7, 7
for idx0 in range(0, 4):
    for idx1 in range(0, 4):
        for idx2 in range(0, 4):

            # real_part = int(xCoord / (numpy.sqrt(2) * 7) * 2**16)
            flag = False
            # if real_part < 0:
            #     flag = True
            # f_real.write(str(real_part) + '\n')
            real_part = FXnum(xCoord / (numpy.sqrt(2) * 7), fam_s_1_16)
            real_part = real_part.toBinaryString()
            real_part = real_part.replace('.', '')
            f_real.write(real_part + '\n')
            real_value = BitArray(bin=real_part)

            # imag_part = int(yCoord / (numpy.sqrt(2) * 7) * 2**16)
            # f_imag.write(str(imag_part) + '\n')
            imag_part = FXnum(yCoord / (numpy.sqrt(2) * 7), fam_s_1_16)
            imag_part = imag_part.toBinaryString()
            imag_part = imag_part.replace('.', '')
            f_imag.write(imag_part + '\n')
            imag_value = BitArray(bin=imag_part)

            index = to_binary_string(idx0*16 + idx1*4 + idx2)
            if flag:
                print index + " --> " + real_part + " + j* " + imag_part

            xPosRef = 300 + 30*xCoord
            yPosRef = 300 - 30*yCoord

            display_enabled = False
            if verification_flag:
                color = (0, 0, 0)  # Black
                size = 5
                display_enabled = True
            else:
                if (index[0:2] == index[2:4]) and (index[2:4] == index[4:6]):
                    color = (255, 0, 0)  # Red
                    if QAM_4:
                        display_enabled = True
                elif index[0:2] == index[4:6]:
                    color = (0, 0, 255)  # Blue
                    if QAM_16:
                        display_enabled = True
                else:
                    color = (0, 0, 0)  # Black
                    if QAM_64:
                        display_enabled = True

            if verification_flag:
                size = 5
                index = index[0:6]
            elif QAM_64:
                size = 5
                index = index[0:6]
            elif QAM_16:
                size = 7
                index = index[0:4]
            elif QAM_4:
                size = 10
                index = index[0:2]

            if display_enabled:
                my_font = pygame.font.SysFont('Comic Sans MS', 4*size)
                text_surface = my_font.render(index, False, (0, 0, 0))
                pygame.draw.circle(screen, color, [xPosRef, yPosRef], size)
                screen.blit(text_surface, (xPosRef-20, yPosRef+5))

            if verification_flag:
                xPosFP = int(300 +
                             30*(float(real_value.int) / 2**16)
                             * 7 * numpy.sqrt(2))
                yPosFP = int(300 -
                             30*(float(imag_value.int) / 2**16)
                             * 7 * numpy.sqrt(2))
                pygame.draw.circle(screen, (255, 0, 0), [xPosFP, yPosFP], size)

            if idx2 == 0:
                xCoord -= 2

            elif idx2 == 1:
                xCoord += 2
                yCoord -= 2

            elif idx2 == 2:
                xCoord -= 2

            elif idx2 == 3:
                xCoord += 2
                yCoord += 2

        if idx1 == 0:
            xCoord -= 4

        elif idx1 == 1:
            xCoord += 4
            yCoord -= 4

        elif idx1 == 2:
            xCoord -= 4

        elif idx1 == 3:
            xCoord += 4
            yCoord += 4

    if idx0 == 0:
        xCoord -= 8

    elif idx0 == 1:
        xCoord += 8
        yCoord -= 8

    elif idx0 == 2:
        xCoord -= 8

    elif idx0 == 3:
        xCoord += 8
        yCoord += 8

pygame.draw.circle(screen,
                   (128, 128, 128),
                   [300, 300],
                   int(7*30*numpy.sqrt(2)) + 1,
                   1)

done = False
while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    pygame.display.flip()

pygame.quit()

# --- End of script --- #
