################################################################################

# --- Script information --- #

# Title :
#   Fixed point, complex multiplication prototyping
#
# Description:
#   Simulates fixed point multiplication to prototype DSP block operation in
#   FPGAs. Its purpose to help to determine the number of fractional and
#   integer bits in the FPGA's fixed point representation.

################################################################################

# --- Imports --- #
from FixedPoint import FXfamily, FXnum          # Fixed point package
import numpy                                    # Numerical computation package


# --- Fixed point representation --- #

fractional_bits = 16
sign_plus_integer_bits = 2                      # One sign bit, two integer bits

fam_signed_1_16 = FXfamily(fractional_bits, sign_plus_integer_bits)

fractional_bits_mult = 2 * fractional_bits      # 30 fractional bits
sign_plus_integer_bits_mult = \
    2 * (sign_plus_integer_bits + fractional_bits) \
    - fractional_bits_mult                      # One sign bit, 5 integer bits

fam_mult_signed_5_30 = \
    FXfamily(fractional_bits_mult, sign_plus_integer_bits_mult)


# --- Complex multiplication of f.p. numbers --- #

# Question: Which bits are useful after multiplication?

a_ref_re = -0.5                                 # Real part of operand "A"
a_ref_im = 0.5                                  # Imaginary part of operand "A"

a_fix_re = FXnum(-0.5, fam_signed_1_16)         # Fixed point version of
a_fix_im = FXnum(0.5, fam_signed_1_16)          # operand "A"

b_ref_re = 0.5                                  # Real part of operand "B"
b_ref_im = 0.5                                  # Imaginary part of operand "B"

b_fix_re = FXnum(0.5, fam_signed_1_16)          # Fixed point version of
b_fix_im = FXnum(0.5, fam_signed_1_16)          # operand "B"

# Extension for multiplication
a_re_ext = FXnum(a_fix_re, fam_mult_signed_5_30)
a_im_ext = FXnum(a_fix_im, fam_mult_signed_5_30)

# Extension for multiplication
b_re_ext = FXnum(b_fix_re, fam_mult_signed_5_30)
b_im_ext = FXnum(b_fix_im, fam_mult_signed_5_30)

# Reference multiplication
a = complex(a_ref_re, a_ref_im)
b = complex(b_ref_re, b_ref_im)
mult_ref_re = numpy.real(a * b)
mult_ref_im = numpy.imag(a * b)

# Fixed point multiplication
partial_res1 = a_re_ext * b_re_ext
partial_res2 = a_im_ext * b_im_ext
mult_fix_re = partial_res1 - partial_res2

partial_res3 = a_re_ext * b_im_ext
partial_res4 = a_im_ext * b_re_ext
mult_fix_im = partial_res3 + partial_res4

# Comparing the results
print "Ref. real part = " + str(mult_ref_re)
print "F.p. real part = " + str(mult_fix_re)
print mult_fix_re.toBinaryString()

mult_re_trunc = FXnum(mult_fix_re, fam_signed_1_16)
print "F.p. real trunc. = " + str(mult_re_trunc)
print mult_re_trunc.toBinaryString()

print ''

print "Ref. imag. part = " + str(mult_ref_im)
print "F.p. imag. part = " + str(mult_fix_im)
print mult_fix_im.toBinaryString()

mult_im_trunc = FXnum(mult_fix_im, fam_signed_1_16)
print "F.p. imag. trunc. = " + str(mult_im_trunc)
print mult_im_trunc.toBinaryString()

# --- End of script --- #
