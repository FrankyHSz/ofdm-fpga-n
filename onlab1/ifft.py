################################################################################

# --- Script information --- #

# Title :
#   IFFT prototyping script
#
# Description:
#   Computes IFFT using the "numpy" numerical package and "spfpm" fixed point
#   binary arithmetic package for comparison and to prototype IFFT operation
#   in FPGA. Numerical error from fixed point representation is estimated and
#   plotted using "matplotlib" package.

################################################################################


# --- Imports --- #

from FixedPoint import FXfamily, FXnum          # Fixed point arithmetic package
import math                                     # Mathematical functions (log2)
import numpy                                    # Numerical computation package
import scipy                                    # Scientific Python package
import random                                   # Random number generation
import matplotlib.pyplot as plt                 # Plots


# --- IFFT properties --- #

IFFT_point_num = 64                             # Number of IFFT points


# --- Number representation --- #

fractional_bits = 16
sign_plus_integer_bits = 2                      # One sign bit, one integer bit
fam_signed_1_16 = FXfamily(fractional_bits, sign_plus_integer_bits)

# Integer bit extension to support inverse DFT summation
extension = math.log(IFFT_point_num, 2)

# Extended format
fam_signed_1_16_ext = \
    FXfamily(fractional_bits, sign_plus_integer_bits + int(extension))


# --- IFFT fixed point coefficients --- #

# List to hold reference coefficients
IFFT_coeffs_ref_real = list(range(IFFT_point_num))
IFFT_coeffs_ref_imag = list(range(IFFT_point_num))

# List to hold fixed p. coeffs
# Real and imaginary parts are saved into different lists because Python's
# "complex(real, imag)" function does not return with an appropriate fixed
# point representation
IFFT_coeffs_fix_real = list(range(IFFT_point_num))
IFFT_coeffs_fix_imag = list(range(IFFT_point_num))

theta_step = 2*numpy.pi / IFFT_point_num        # Phase step to generate coeffs

# Complex coefficients generated in Euler's form:
# unit long vectors with theta phase
idx = -1
for theta in numpy.arange(0, 2*numpy.pi, theta_step):
    idx += 1                                    # Index of current iteration

    real_part_ref = numpy.cos(theta)            # Real part as floating point
    real_part_fix = FXnum(real_part_ref,        # Real part of the coefficients
                          fam_signed_1_16)      # as fixed point

    imag_part_ref = numpy.sin(theta)            # Imaginary part as floating p.
    imag_part_fix = FXnum(imag_part_ref,        # Imaginary part of coefficients
                          fam_signed_1_16)      # as fixed point

    IFFT_coeffs_ref_real[idx] = real_part_ref
    IFFT_coeffs_ref_imag[idx] = imag_part_ref

    IFFT_coeffs_fix_real[idx] = real_part_fix
    IFFT_coeffs_fix_imag[idx] = imag_part_fix


# --- IFFT input samples --- #

# List to hold symbols (complex signal amplitudes)
symbols_ref = list(range(IFFT_point_num))

# Lists to hold symbols represented as fixed point, complex numbers
# Real and imaginary parts are saved into different lists because Python's
# "complex(real, imag)" function does not return with an appropriate fixed
# point representation
symbols_fix_real = list(range(IFFT_point_num))
symbols_fix_imag = list(range(IFFT_point_num))

# Random symbol generation
# Numbers are in range of 0 to 1
for idx in range(len(symbols_ref)):
    real_part_ref = random.uniform(-1, 1)       # Real part of ref. symbols
    real_part_fix = FXnum(real_part_ref,        # Real part of fixed point
                          fam_signed_1_16)      # symbols

    imag_part_ref = random.uniform(-1, 1)       # Imaginary part of ref. symbols
    imag_part_fix = FXnum(imag_part_ref,        # Imaginary part of fixed point
                          fam_signed_1_16)      # symbols

    symbols_ref[idx] = complex(real_part_ref, imag_part_ref)
    symbols_fix_real[idx] = real_part_fix
    symbols_fix_imag[idx] = imag_part_fix


# --- Computing IFFT --- #

# Computing reference IFFT
time_vector_ref_num = numpy.fft.\
    ifft(symbols_ref)
time_vector_ref_sci = scipy.ifft(symbols_ref)


# Vectors to hold the results of reference inverse DFT
time_vector_ref_real = list(range(IFFT_point_num))
time_vector_ref_imag = list(range(IFFT_point_num))

# Vectors to hold the results of fixed point inverse DFT
time_vector_fix_real = list(range(IFFT_point_num))
time_vector_fix_imag = list(range(IFFT_point_num))

for idx_time in range(IFFT_point_num):          # Computing inverse DFT

    # Reference sums
    DFT_sum_ref_real = 0
    DFT_sum_ref_imag = 0

    # Extended fixed point variables to prevent overflow whilst summation
    DFT_sum_fix_real = FXnum(0, fam_signed_1_16_ext)
    DFT_sum_fix_imag = FXnum(0, fam_signed_1_16_ext)

    for idx in range(IFFT_point_num):           # Computing one value in time

        # Index of actual coefficient
        # In math form coeffs would be exp{2pi*i*mk/n}
        # where m = idx_time, k = idx, n = IFFT_point_num
        # In this script, coeffs are computed as multiples of exp{2pi*i/n} and
        # stored in list. Indexing the list with e.g. "p" returns exp{2pi*i*p/n}
        # where p = 0, 1, 2, ... n-1 (or IFFT_point_num - 1).
        # In case of p = m*k we need to ensure that m*k do not exceeds the list.
        # Because of the periodicity of exp{2pi*i*p/n} we can write that
        # p = (m * k) mod n, hence the line below
        coeffs_idx = (idx_time * idx) % IFFT_point_num

        # Generating complex products
        # (a + jb) * (c + jd) = (ac - bd) + j(ad + bc)
        product_ref_real = \
            numpy.real(symbols_ref[idx]) * IFFT_coeffs_ref_real[coeffs_idx] \
            - numpy.imag(symbols_ref[idx]) * IFFT_coeffs_ref_imag[coeffs_idx]
        product_ref_imag = \
            numpy.real(symbols_ref[idx]) * IFFT_coeffs_ref_imag[coeffs_idx] \
            + numpy.imag(symbols_ref[idx]) * IFFT_coeffs_ref_real[coeffs_idx]

        product_fix_real = \
            symbols_fix_real[idx] * IFFT_coeffs_fix_real[coeffs_idx] \
            - symbols_fix_imag[idx] * IFFT_coeffs_fix_imag[coeffs_idx]
        product_fix_imag = \
            symbols_fix_real[idx] * IFFT_coeffs_fix_imag[coeffs_idx] \
            + symbols_fix_imag[idx] * IFFT_coeffs_fix_real[coeffs_idx]

        # Inverse DFT summation to produce actual value in time vector
        # Vector elements are converted into extended fixed point format in
        # order to prevent overflow
        DFT_sum_fix_real += FXnum(product_fix_real, fam_signed_1_16_ext)
        DFT_sum_fix_imag += FXnum(product_fix_imag, fam_signed_1_16_ext)

        DFT_sum_ref_real += product_ref_real
        DFT_sum_ref_imag += product_ref_imag

    # Scaling back the sum to get the actual value in time vector
    time_vector_fix_real[idx_time] = DFT_sum_fix_real / IFFT_point_num
    time_vector_fix_imag[idx_time] = DFT_sum_fix_imag / IFFT_point_num

    time_vector_ref_real[idx_time] = DFT_sum_ref_real / IFFT_point_num
    time_vector_ref_imag[idx_time] = DFT_sum_ref_imag / IFFT_point_num


# --- Displaying numerical error --- #

time_axis = range(IFFT_point_num)               # Horizontal axis for plots

# plt.figure(1)

# plt.subplot(211)
# plt.plot(
#     time_axis, IFFT_coeffs_ref_real,
#     time_axis, IFFT_coeffs_fix_real, 'r--')

# plt.subplot(212)
# plt.plot(
#     time_axis, IFFT_coeffs_ref_imag,
#     time_axis, IFFT_coeffs_fix_imag, 'r--')


# plt.figure(2)

# plt.subplot(211)
# plt.plot(
#     time_axis, numpy.real(symbols_ref),
#     time_axis, symbols_fix_real, 'r--')

# plt.subplot(212)
# plt.plot(
#     time_axis, numpy.imag(symbols_ref),
#     time_axis, symbols_fix_imag, 'r--')


# plt.figure(3)

# plt.subplot(211)
# plt.plot(
#     time_axis, numpy.real(time_vector_ref_sci),
#     time_axis, numpy.real(time_vector_ref_num), 'g--',
#     time_axis, time_vector_ref_real,            # Plotting real part of time
#     time_axis, time_vector_fix_real, 'r--')     # vectors

# plt.subplot(212)
# plt.plot(
#     time_axis, numpy.imag(time_vector_ref_sci),
#     time_axis, numpy.imag(time_vector_ref_num), 'g--',
#     time_axis, time_vector_ref_imag,            # Plotting imaginary part of
#     time_axis, time_vector_fix_imag, 'r--')     # time vectors

time_vector_error_real = numpy.real(time_vector_ref_sci) - time_vector_fix_real
time_vector_error_real /= numpy.real(time_vector_ref_sci)
time_vector_error_imag = numpy.imag(time_vector_ref_sci) - time_vector_fix_imag
time_vector_error_imag /= numpy.imag(time_vector_ref_sci)

plt.figure(1)

plt.plot(
    time_axis, time_vector_error_real, label='Error of real part')
plt.plot(
    time_axis, time_vector_error_imag, label='Error of imag. part')

plt.title('Fixed point IFFT error relative to SciPy IFFT')
plt.xlabel('IFFT samples')
plt.ylabel('Relative error [%]')
plt.legend()
plt.grid(True)

plt.show()


# --- End of script --- #
