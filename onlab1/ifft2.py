################################################################################

# --- Script information --- #

# Title :
#   IFFT prototyping script #2
#
# Description:
#   Computes reference IFFT for VHDL testbench, loads the values computed by
#   FPGA and plots them and their difference out.

################################################################################


# --- Imports --- #

import scipy                                # Scientific Python package
from bitstring import BitArray              # Binary string to int conversion
import matplotlib.pyplot as plt             # Plots


# --- Input values --- #

IFFT_point_num = 8
input_vector = list(range(IFFT_point_num))

for idx in range(IFFT_point_num):
    input_vector[idx] = idx


# --- Computation of IFFT values --- #

ifft_ref = scipy.ifft(input_vector)


# --- Loading the IFFT values computed by FPGA --- #

ifft_fpga = list(range(IFFT_point_num))
f = open("ifft_fpga.txt", "r")

for idx in range(IFFT_point_num):
    line = f.readline()
    num_value = BitArray(bin=line)
    ifft_fpga[idx] = num_value.int


# --- Plotting them and their difference --- #
plt.figure(1)

plt.plot(ifft_ref, label='Reference IFFT')
plt.plot(ifft_fpga, label='Fixed point IFFT')

plt.title('Fixed point IFFT vs. SciPy IFFT')
plt.xlabel('IFFT samples')
plt.ylabel('IFFT values')
plt.legend()
plt.grid(True)

plt.show()

# --- End of script --- #
